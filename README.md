# FairPlay Links

Страница на developer.apple.com с набором ссылок по FairPlay

https://developer.apple.com/streaming/fps/

Краткий обзор FairPlay Streaming

https://developer.apple.com/streaming/fps/FairPlayStreamingOverview.pdf

SDK - набор примеров кода, programming guide для клиентской и серверной части

https://developer.apple.com/services-account/download?path=/Developer_Tools/FairPlay_Streaming_Server_SDK/FairPlay_Streaming_Server_SDK_4.4.zip


WWDC-сессии

https://developer.apple.com/videos/play/wwdc2015/502/
https://developer.apple.com/videos/play/wwdc2018/507/
